mysql> create database Week5Assignment1;
Query OK, 1 row affected (0.18 sec)


mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| hcl                |
| mysql              |
| performance_schema |
| sys                |
| week5assignment1   |
+--------------------+
6 rows in set (0.00 sec)

mysql> use week5assignment1;
Database changed
mysql> show tables;
Empty set (0.00 sec)

mysql> CREATE TABLE employee(id INTEGER, name VARCHAR(10),department VARCHAR(10),salary FLOAT);
Query OK, 0 rows affected (1.77 sec)

mysql> show tables;
+----------------------------+
| Tables_in_week5assignment1 |
+----------------------------+
| employee                   |
+----------------------------+
1 row in set (0.00 sec)

mysql> INSERT INTO employee values(1,'Aman','IT',12000),(2,'Bhuvan','HR',15000),
(3,'Chandan','Admin',18000);
Query OK, 3 rows affected (0.14 sec)
Records: 3  Duplicates: 0  Warnings: 0

mysql> select * from employee;
+------+---------+------------+--------+
| id   | name    | department | salary |
+------+---------+------------+--------+
|    1 | Aman    | IT         |  12000 |
|    2 | Bhuvan  | HR         |  15000 |
|    3 | Chandan | Admin      |  18000 |
+------+---------+------------+--------+
3 rows in set (0.02 sec)

mysql> select SUM(salary) from employee;
+-------------+
| SUM(salary) |
+-------------+
|       45000 |
+-------------+
1 row in set (0.00 sec)

mysql> select AVG(salary) from employee;
+-------------+
| AVG(salary) |
+-------------+
|       15000 |
+-------------+
1 row in set (0.02 sec)

mysql> select MIN(salary) from employee;
+-------------+
| MIN(salary) |
+-------------+
|       12000 |
+-------------+
1 row in set (0.08 sec)

mysql> select MAX(salary) from employee;
+-------------+
| MAX(salary) |
+-------------+
|       18000 |
+-------------+
1 row in set (0.00 sec)

mysql> UPDATE employee SET salary=salary+(salary*30/100);
Query OK, 3 rows affected (0.08 sec)
Rows matched: 3  Changed: 3  Warnings: 0

mysql> select * from employee;
+------+---------+------------+--------+
| id   | name    | department | salary |
+------+---------+------------+--------+
|    1 | Aman    | IT         |  15600 |
|    2 | Bhuvan  | HR         |  19500 |
|    3 | Chandan | Admin      |  23400 |
+------+---------+------------+--------+
3 rows in set (0.00 sec)

mysql> TRUNCATE TABLE employee;
Query OK, 0 rows affected (0.20 sec)

mysql> select * from employee;
Empty set (0.00 sec)

mysql>